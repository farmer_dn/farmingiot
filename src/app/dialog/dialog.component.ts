import { Component, Inject } from '@angular/core';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';

/**
 * @title Dialog Overview
 */
@Component({
    selector: 'dialog-component',
    templateUrl: './dialog.component.html'
})
export class DialogInputComponent {
    typeName: string;
    name: string;
    deviceId: string;

    constructor(public dialog: MdDialog) { }

    openDialog(): void {
        let dialogRef = this.dialog.open(DetailDialog, {
            width: '450px',
            data: { typeName: this.typeName, name: this.name, deviceId: this.deviceId }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(result);
            this.name = result.deviceName;
            this.deviceId = result.deviceId;
        });
    }

}

@Component({
    selector: 'dialog-detail',
    templateUrl: './dialog-detail.html',
})
export class DetailDialog {
    constructor(
        public dialogRef: MdDialogRef<DetailDialog>,
        @Inject(MD_DIALOG_DATA) public data: any) { }

    onNoClick(): void {
        this.dialogRef.close();
    }

}