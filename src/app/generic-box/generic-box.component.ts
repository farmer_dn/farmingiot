import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-generic-box',
  templateUrl: './generic-box.component.html'
})
export class GenericBoxComponent implements OnInit {
  @Input()
  genericBox: { deviceName: 'Device 1', deviceId: 'Device 1 Content', deviceType: "Test" };
  constructor() { }

  ngOnInit() {
    if (!this.genericBox) {
      this.genericBox = { deviceName: 'Device 1', deviceId: 'Device 1 Content', deviceType: "Test" }
    }
  }
}