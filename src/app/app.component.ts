import { Component, Output, EventEmitter } from '@angular/core';
import {EmptyEnumClass, TypeCheckEnum} from './type-check/type-check-enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app demo Farming IOT Control';
  devices = [
    {
      icon: 'opacity',
      name: 'wind',
      updated: new Date('1/1/16'),
    },
    {
      icon: 'hot_tub',
      name: 'meter',
      updated: new Date('1/1/16'),
    },
    {
      icon: 'beach_access',
      name: 'rain',
      updated: new Date('1/1/16'),
    },
    {
      icon: 'ac_unit',
      name: 'snow',
      updated: new Date('1/1/16'),
    }
  ];
  notes = [
    {
      name: 'Vacation Itinerary',
      updated: new Date('2/20/16'),
    },
    {
      name: 'Kitchen Remodel',
      updated: new Date('1/18/16'),
    }
  ];
  typeCheck = TypeCheckEnum;
  highlight:Array<string> = ['',''];
  warningMessage:string = '';

  @Output()
  droppedItemType:EventEmitter<any> = new EventEmitter();

  constructor() { }

  private addDropItem(event,type){
    if (type === TypeCheckEnum.Device){
      if (event.type === TypeCheckEnum.Device){
      }else{
        //this.warningMessage = "Technically a round shape will fit in a square shape, try to match them"
      }
    }
    if (type === TypeCheckEnum.Material){
      if (event.type === TypeCheckEnum.Material){
        // this.itemsDroppedRound.push(event);
        // this.droppedItemType.emit(event.type);
        // this.warningMessage = '';
      }else {
        // this.warningMessage ="A square shape will not fit in a round shape."
      }
    }
  }

  private dragEnter(event,type){
    if (event.type !== type){
      this.highlight[type] = 'badHighlight';
    } else {
      this.highlight[type] = 'highlight';
    }
  }
  private dragLeave(){
    this.highlight=['',''];
  }

  private startDrag(item){
    console.log('Begining to drag item: ' + item.name);
  }
}
