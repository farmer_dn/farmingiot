import { Component, Output, EventEmitter } from '@angular/core';
import {EmptyEnumClass, TypeCheckEnum} from '../type-check-enum';

@Component({
  selector: 'app-drop-area-type-check',
  template: `
    <p>
      Match Shapes<br />
      {{warningMessage}}
    </p>
    <div dropDirective (dropEvent)="addDropItem($event,typeCheck.Round)" (dragenterEvent)="dragEnter($event,typeCheck.Round)" (dragleaveEvent)="dragLeave()" class="droppable round-hole" [ngClass]="highlight[typeCheck.Round]">
    	<app-generic-box *ngFor="let item of itemsDroppedRound" [genericBox]='item'></app-generic-box>
    </div>
    <div dropDirective (dropEvent)="addDropItem($event,typeCheck.Square)" (dragenterEvent)="dragEnter($event,typeCheck.Square)" (dragleaveEvent)="dragLeave()" class="droppable square-hole" [ngClass]="highlight[typeCheck.Square]" >
      <app-generic-box *ngFor="let item of itemsDroppedSquare" [genericBox]='item'></app-generic-box>
    </div>
  `,
  styleUrls: ['./drop-area-type-check.component.css']
})
export class DropAreaTypeCheckComponent{
	itemsDroppedRound:Array<any> = [];
  itemsDroppedSquare:Array<any> = [];
  warningMessage:string = '';
  highlight:Array<string> = ['',''];
  typeCheck = TypeCheckEnum;
  @Output()
  droppedItemType:EventEmitter<any> = new EventEmitter();

  constructor() { }
  
   addDropItem(event,type){
    if (type === TypeCheckEnum.Device){
      if (event.type === TypeCheckEnum.Device){
        this.itemsDroppedSquare.push(event);
        this.droppedItemType.emit(event.type);
        this.warningMessage = '';
      }else{
        this.warningMessage = "Do not same type!"
      }
    }
    if (type === TypeCheckEnum.Material){
      if (event.type === TypeCheckEnum.Material){
        this.itemsDroppedRound.push(event);
        this.droppedItemType.emit(event.type);
        this.warningMessage = '';
      }else {
        this.warningMessage = "Do not same type!"
      }
    }
  }

  dragEnter(event,type){
    if (event.type !== type){
      this.highlight[type] = 'badHighlight';
    } else {
      this.highlight[type] = 'highlight';
    }
  }
  
  dragLeave(){
    this.highlight=['',''];
  }
}