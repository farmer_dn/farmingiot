import { Component } from '@angular/core';
import { TypeCheckEnum } from './type-check-enum';

@Component({
  selector: 'app-type-check',
  templateUrl: './type-check.html'
})
export class TypeCheckComponent {
  title = '';
  dropItemType:any;
  dropItemTypeCheck(event){
    this.dropItemType = event;
  }
}