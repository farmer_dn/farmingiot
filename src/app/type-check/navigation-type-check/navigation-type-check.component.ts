import { Component, OnInit, Input } from '@angular/core';
import { EmptyEnumClass, TypeCheckEnum } from '../type-check-enum';

@Component({
  selector: 'app-navigation-type-check',
  templateUrl: './template.html',
  styleUrls: ['./navigation-type-check.component.css']
})
export class NavigationTypeCheckComponent {
  @Input()
  dropItemType: any;
  pegType = TypeCheckEnum;
  itemsToDrop: Array<Object> = [
    {
      deviceType: 'Device 1',
      deviceName: 'measure moisture',
      type: TypeCheckEnum.Device
    },
    {
      deviceType: 'Device 2',
      deviceName: 'Brightness',
      type: TypeCheckEnum.Material
    },
  ]
  constructor() { }


  releaseDrop(event) {
    let index = this.itemsToDrop.indexOf(event);
    if (index >= 0) {
      setTimeout(() => { (this.checkType(event, index), 100) });
    }
  }
  checkType(event, index) {
    if (event.type === this.dropItemType) {
      // this.itemsToDrop.splice(index,1);
      alert(event.type);
    }
  }
  startDrag(item) {
    console.log('Begining to drag item: ' + item);
  }

}