import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-navigation',
	template: `
    <div *ngFor="let item of itemsToDrop" [dragDirective]='item' [dragHightlight]="'highlight'" (releaseDrop)="releaseDrop($event)" class="dragItem" >
      <app-generic-box [genericBox]='item'></app-generic-box>
    </div>
  `,
	styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
	itemsToDrop: Array<Object> = [
		{
			name: 'Item to drop 1',
			content: 'desctiption 1'
		},
		{
			name: 'Item to drop 2',
			content: 'desctiption 2'
		},
		{
			name: 'Item to drop 3',
			content: 'desctiption 3'
		},
	]
	constructor() { }

	ngOnInit() {
	}
	
	releaseDrop(event) {
		let index = this.itemsToDrop.indexOf(event);
		if (index >= 0) {
			this.itemsToDrop.splice(index, 1);
		}
	}

}
