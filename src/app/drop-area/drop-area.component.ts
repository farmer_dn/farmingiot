import { Component, Inject } from '@angular/core';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';

@Component({
    selector: 'app-drop-area',
    templateUrl: './drop-area.component.html',
    styleUrls: ['./drop-area.component.css']
})
export class DropAreaComponent {
    itemsDropped:  FirebaseListObservable<any[]>;//Array<any> = [];
    items: FirebaseListObservable<any[]>;
    typeName: string;
    name: string;
    deviceId: string;

    constructor(public dialog: MdDialog, public afAuth: AngularFireAuth, public af: AngularFireDatabase) { 
        this.itemsDropped = af.list('/devices', {
            query: {
              limitToLast: 50
            }
          });
    }

    openDialog(): void {
        let dialogRef = this.dialog.open(DetailDialog, {
            width: '450px',
            data: { typeName: this.typeName, name: this.name, deviceId: this.deviceId }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(result);
            if(result!=null){
                this.name = result.deviceName;
                this.deviceId = result.deviceId;
                this.itemsDropped.push(result);
                // this.items.push(result);
            }
        });
    }

    addDropItem(event) {
        if(event.name !== ""){
            // open dialog add info
            this.typeName = event.deviceType;
            this.name = "";
            this.deviceId = "";
            this.openDialog();
        }else{
            this.itemsDropped.push(event);
        }        
    }
}

@Component({
    selector: 'dialog-detail',
    templateUrl: './dialog-detail.html',
})
export class DetailDialog {
    constructor(
        public dialogRef: MdDialogRef<DetailDialog>,
        @Inject(MD_DIALOG_DATA) public data: any) { }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
